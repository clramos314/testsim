#!/bin/bash

export WD=$(pwd)

echo "Downloading genome..."
mkdir -p res/genome
cd res/genome
wget -O ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz -P res/genome
cd $WD

#sampleids=$(ls data/*.fastq.gz | cut -d "_" -f1 | sed 's:data/::' | sort | uniq)
#alternative
sampleids=$(ls data/*.fastq.gz | cut -d "_" -f1 | cut -d "/" -f2 | sort | uniq)

echo "Running STAR index..."
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9
echo

for sampleid in $sampleids
do
    bash scripts/analyse_sample.sh $sampleid 
done

echo "Running multiqc..."
mkdir -p out/multiqc
multiqc -f -o out/multiqc $WD
echo

echo "Pipeline finished"
